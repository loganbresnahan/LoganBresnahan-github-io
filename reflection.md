[Logan Bresnahan's website](https://loganbresnahan.github.io/index.html)

***What did you learn about CSS padding, borders, and margin by doing this challenge?***
I learned they are confusing and don't always do what I expect. The border attribute is easy enough, just an outline of your element,
but padding and margin are another story. Padding, will increase the element beyond the margin if you make them big enough which I 
still don't get. Margin, in my mind, should push other elements away from it but this doesn't seem to be the case.
If I'm being honest, I need to keep practicing because every time I think I have it figured out CSS slaps me in the face.

***What did you learn about CSS positioning?***
This seems fairly straight forward if you are using the attributes fixed or absolute....I think. I don't understand positioning at all. 
If I make everything fixed and can set elements manually with top, bottom, right, and left; but this really limited the adaptiveness 
of the website. Regardless, I did that anyway. Once again, this is a case of me not knowing how to properly control these attributes.

***What aspects of your design did you find easiest to implement? What was most difficult?***
The colors and size of elements were easy. Everything else I find to be difficult. 

***What did you learn about adding and formatting elements with CSS in this challenge?***
I did learn a lot and I got my elements into the desired positions, but I feel like I MacGyvered the crap out of my site.
Metaphorically, this thing is held together with rubber bands and toothpicks. What I really need to learn now is positioning so that 
my site is reactive to different browser sizes.